# Hectic

The official repository for Hectic, the planner that will help you manage and
organize your daily plans

<script src="https://liberapay.com/kdeleteme/widgets/button.js"></script>
<noscript>
    <a href="https://liberapay.com/kdeleteme/donate">
        <img alt="Donate using Liberapay" 
        src="https://liberapay.com/assets/widgets/donate.svg">
    </a>
</noscript>

## Roadmap

Hectic is currently in a working state.

- [ ] Migrate to Jetpack Compose

### Features to Add (in no specific order)
- [X] Switch between light and dark themes
- [ ] Goal Setting
- [ ] Android home screen widget

### Features to consider (in no specific order)
- [ ] Text formatting
- [ ] Allow users to customize theming
- [ ] Google Calendar/{other calendar solution here} integration (?)

## License

Copyright (C) 2021  Kent Delante <leftybournes@pm.me>

Hectic is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hectic is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

See more at [LICENSE](LICENSE)
