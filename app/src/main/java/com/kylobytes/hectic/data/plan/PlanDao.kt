// Copyright (C) 2018  Kent Delante

// This file is part of Hectic
// Hectic is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Hectic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Hectic. If not, see <http://www.gnu.org/licenses/>.

package com.kylobytes.hectic.data.plan

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface PlanDao {
    @Query("SELECT * FROM plans WHERE scheduled_date = :date ORDER BY start_time ASC")
    fun loadPlansByDate(date: Long): LiveData<List<Plan>>

    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(plan: Plan): Long

    @Transaction
    @Update
    suspend fun update(plan: Plan)

    @Transaction
    @Delete
    suspend fun delete(plan: Plan)
}