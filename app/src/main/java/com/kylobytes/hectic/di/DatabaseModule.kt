/*
 * Copyright (C) 2021  Kent Delante <leftybournes@pm.me>
 *
 * This file is part of Hectic.
 *
 * Hectic  is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Hectic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hectic.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kylobytes.hectic.di

import android.content.Context
import androidx.room.Room
import com.kylobytes.hectic.data.AppDatabase
import com.kylobytes.hectic.data.plan.PlanDao
import com.kylobytes.hectic.data.plan.PlanRepository
import com.kylobytes.hectic.util.Constants.APPLICATION_DB
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
internal object DatabaseModule {
    @Provides
    fun provideDatabase(@ApplicationContext context: Context) =
        Room.databaseBuilder(
            context.applicationContext,
            AppDatabase::class.java,
            APPLICATION_DB
        ).build()

    @Provides
    fun providePlanDao(database: AppDatabase) = database.planDao()

    @Provides
    fun providePlanRepository(planDao: PlanDao) = PlanRepository(planDao)
}