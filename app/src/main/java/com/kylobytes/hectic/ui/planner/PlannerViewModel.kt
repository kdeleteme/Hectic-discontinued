/*
 * Copyright (C) 2021  Kent Delante <leftybournes@pm.me>
 *
 * This file is part of Hectic.
 *
 * Hectic  is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Hectic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hectic.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.kylobytes.hectic.ui.planner

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kylobytes.hectic.data.plan.PlanRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

@HiltViewModel
class PlannerViewModel @Inject constructor(
    private val planRepository: PlanRepository
) : ViewModel() {

    private val dateFormatter = SimpleDateFormat(
        "EEEE, dd MMM yyyy",
        Locale.getDefault()
    )

    // The current date the planner view is currently on.
    // Doesn't necessarily mean today. Could be the date set by the user.
    private val _currentDate = MutableLiveData(formatDate(presentDate.time))
    val currentDate = _currentDate

    private val presentDate: Calendar
        get() {
            val timeZone = TimeZone.getDefault()
            return Calendar.getInstance(timeZone)
        }

    private fun formatDate(date: Date): String =
        dateFormatter.format(date)
}